from django.urls import path
from projects.views import list_projects, detail_projects


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", detail_projects, name="show_project"),
]
