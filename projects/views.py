from django.shortcuts import render
from projects.models import Project
from django.contrib.auth.decorators import login_required


# Create your views here.


@login_required
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "project": project,
    }
    return render(request, "projects/list.html", context)


@login_required
def detail_projects(request, id):
    detail = Project.objects.get(id=id)
    context = {
        "detail": detail,
    }
    return render(request, "projects/detail.html", context)
